# LFBrain - análise dos concursos da lotofácil utilizando o SciKit-Learn e tensorflow (skflow)
# Módulo principal
# by Amaury Carvalho, junho/2018
# amauryspires@gmail.com

from LFPack.Lotofacil import Lotofacil
from LFPack.SKLHelper import SKLHelper

print("Análise do histórico dos concursos da lotofácil")
print("com base em métodos de classificação e regressão")

lf = Lotofacil()

resp = input("\nDeseja fazer o download atualizado dos concursos da lotofácil (S/N)?")

if resp.upper() == "S":
    print("\nFazendo o download dos concursos da lotofácil no portal da Caixa Economica Federal...")
    lf.Download()
    print("Download concluído")

print("\nCarregando o arquivo d_lotfac.csv...")

lf.Load()

print( lf.getRawData() )
print( lf.getCombinData() )
print( lf.getCsnData() )

print( "=%i concursos carregados" % len(lf.getCombinData()) )

print("\nCriando o dataset X e Y da posição 1 dos concursos...")

x, y = lf.getDataSetXY(0)

print(x)
print(y)

print("\nAnalisando os scores dos vários métodos de análise\n")

skl = SKLHelper()
estimators = skl.getEstimators()

for estimator in estimators:
    print(skl.getName(estimator))
    score = skl.getScore(estimator, x, y)
    print("=Score: %f" % score)

