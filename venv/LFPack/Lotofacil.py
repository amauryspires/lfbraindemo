# Classe Lotofácil
# By: Amaury Carvalho, junho/2018
# amauryspires@gmail.com

import requests
import wget

from io import BytesIO
from zipfile import ZipFile

import copy
import pandas
import csv

from sklearn.preprocessing import StandardScaler
import skflow
import numpy as np

class Lotofacil():
    def __init__(self):
        self.combin_length = 15
        self.combin_min_number = 1
        self.combin_max_number = 25
        self.raw_data = []
        self.combin_data = []
        self.csn_data = []

    def getCombinParams(self):
        return self.combin_length, self.combin_min_number, self.combin_max_number

    def Download(self):
        # baixando os últimos concursos
        file = requests.get("http://www1.caixa.gov.br/loterias/_arquivos/loterias/D_lotfac.zip")

        # descompactando o arquivo
        zipfile = ZipFile(BytesIO(file.content))
        #print(zipfile.namelist())

        # carregando os concursos na memória
        unzip = zipfile.open("D_LOTFAC.HTM").read()

        # fazendo o parse do html
        parsed = pandas.read_html(unzip)
        df = pandas.concat(parsed)

        list = []

        for row in df.iterrows():
            if (not row[1][2] != row[1][2]):
                if (row[1][0] == "Concurso"):
                    list.append(
                        [row[1][0], row[1][1], row[1][2], row[1][3], row[1][4], row[1][5], row[1][6],
                         row[1][7], row[1][8], row[1][9], row[1][10], row[1][11],
                         row[1][12], row[1][13], row[1][14], row[1][15], row[1][16]])
                else:
                    list.append(
                        [int(row[1][0]), row[1][1], int(row[1][2]), int(row[1][3]), int(row[1][4]), int(row[1][5]),
                         int(row[1][6]),
                         int(row[1][7]), int(row[1][8]), int(row[1][9]), int(row[1][10]), int(row[1][11]),
                         int(row[1][12]), int(row[1][13]), int(row[1][14]), int(row[1][15]), int(row[1][16])])

        # criando o csv
        name = "./d_lotfac.csv"
        with open(name, "w") as output:
            writer = csv.writer(output, lineterminator='\n')
            writer.writerows(list)
        return name

    def Load(self):
        with open("./d_lotfac.csv", "r") as input:
            reader = csv.reader(input, lineterminator='\n')
            self.raw_data = list(reader)

        self.combin_data = copy.deepcopy(self.raw_data)
        del self.combin_data[0:1]
        for row in self.combin_data:
            del row[0:2]
            for k in range(len(row)):
                row[k] = int(row[k])
            row.sort()

        self.csn_data = []
        for row in self.combin_data:
            self.csn_data.append( 0 )

    def getRawData(self):
        return self.raw_data

    def getCombinData(self):
        return self.combin_data

    def getCsnData(self):
        return self.csn_data

    def getDataSetXY(self, position):
        x = []
        y = []
        i = 0
        a = 0

        for row in self.combin_data:
            x.append([i, a])
            a = row[position]
            y.append(a)
            i += 1

        return x, y

