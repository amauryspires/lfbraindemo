# Classe Wrapper do SKNN
# By: Amaury Carvalho, junho/2018
# amauryspires@gmail.com

import numpy as np
from sknn.mlp import Regressor, Layer
from sklearn.preprocessing import MinMaxScaler
from sklearn import metrics

class SKNNWrapper():
    """SKNN Wrapper"""

    def __init__(self, max_iter):
        self.max_iter = max_iter

        self.mms = MinMaxScaler()

        self.estimator = Regressor( layers=[ Layer("Rectifier", units=150),
                                             Layer("Rectifier", units=100),
                                             Layer("Rectifier", units=50),
                                             Layer("Rectifier", units=25),
                                             Layer("Rectifier", units=10),
                                             Layer("Linear") ], learning_rate=0.02, n_iter = self.max_iter )

    def fit(self, x, y):
        x_train = np.array(x)
        y_train = np.array(y)
        self.estimator.fit( x_train, y_train )

    def score(self, x, y):
        x_test = np.array(x)
        y_test = np.array(y)
        score = metrics.accuracy_score(y_test, self.estimator.predict(x_test))
        return score

    def predict(self, x):
        x_test = np.array(x)
        #y_test = np.zeros((len(x_test), 1))
        predicted_values = self.estimator.predict(x_test)
        return predicted_values


