# Classe Wrapper do TensorFlow
# By: Amaury Carvalho, junho/2018
# amauryspires@gmail.com

import numpy as np
import tensorflow as tf
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from tensorflow.python.client import device_lib

class TFWrapper():
    """TensorFlow Wrapper"""

    def __init__(self, layers, max_iter):
        self.shapes = 2
        self.labels = 1
        self.max_iter = max_iter

        self.mms = MinMaxScaler()
        self.feature_columns = [tf.feature_column.numeric_column("x", shape=[self.shapes])]

        tf.logging.set_verbosity(tf.logging.ERROR)
        self.estimator = tf.estimator.DNNRegressor(feature_columns=self.feature_columns,
                                                   hidden_units=layers, label_dimension=self.labels)

    def fit(self, x, y):
        x_train = np.array(x) #self.mms.fit_transform( np.array(x) )
        y_train = np.array(y)
        train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": x_train},
            y=y_train,
            num_epochs=None,
            shuffle=True)
        self.estimator.train(input_fn=train_input_fn, steps=self.max_iter)

    def score(self, x, y):
        x_test = np.array(x) #self.mms.transform(np.array(x))
        y_test = np.array(y)
        test_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": x_test},
            y=y_test,
            num_epochs=1,
            shuffle=False)
        eva = self.estimator.evaluate(input_fn=test_input_fn)
        loss = eva["average_loss"]
        gain = 1.0 - loss
        return gain

    def predict(self, x):
        x_test = np.array(x) #self.mms.transform(np.array(x))
        y_test = np.zeros((len(x_test), 1))
        predict_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"x": x_test},
            y=y_test,
            num_epochs=1,
            shuffle=False)
        predictions = list(self.estimator.predict(input_fn=predict_input_fn))
        predicted_values = [p["predictions"][0] for p in predictions]
        return predicted_values

    def getDevices(self):
        local_device_protos = device_lib.list_local_devices()
        return [x.name for x in local_device_protos if x.device_type == 'GPU' or x.device_type == 'CPU']

    def setDevice(self, device):
        tf.device( device )

