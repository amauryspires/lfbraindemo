# Classe Helper do SciKit-Learn
# By: Amaury Carvalho, junho/2018
# amauryspires@gmail.com

from sklearn import svm, metrics, model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn import linear_model
from sklearn.neighbors import KNeighborsRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.externals import joblib
from LFPack.TFWrapper import TFWrapper
#from LFPack.SKNNWrapper import SKNNWrapper
import skflow
import numpy as np

class SKLHelper():
    def __init__(self):
        self.scaler = StandardScaler()
        self.estimators = [ svm.SVC(gamma=0.001, C=100.),
                            svm.SVR(gamma=0.001, C=100.),
                            LogisticRegression(),
                            GaussianNB(),
                            linear_model.SGDRegressor(max_iter=1000, n_iter=None),
                            KNeighborsRegressor(),
                            GaussianProcessRegressor(),
                            MLPRegressor(hidden_layer_sizes=(150, 100, 50, 25, 10), max_iter=50,
                                         random_state=0, solver='lbfgs'),
                            #SKNNWrapper(max_iter = 50),
                            TFWrapper(layers = [150, 100, 50, 25, 10], max_iter = 50) ]

    def getName(self, estimator):
        lines = estimator.__doc__.splitlines()
        title = lines[0]
        if title == '':
            title = lines[1]
        return title.strip()

    def getScore(self, estimator, x, y):
        self.scaler.fit(x)
        x_scaled = self.scaler.transform(x)
        estimator.fit(x_scaled, y)
        # score = metrics.accuracy_score(y, estimator.predict(x))
        # cv_score = model_selection.cross_val_score(estimator, x_scaled, y, scoring='accuracy')
        score = estimator.score(x_scaled, y)
        return score

    def getPredict(self, estimator, x):
        x_scaled = self.scaler.transform(x)
        return estimator.predict( [ x_scaled ] )

    def getEstimators(self):
        return self.estimators

    def saveEstimator(self, estimator):
        joblib.dump(estimator, "./estimator.pk1")

    def loadEstimator(self):
        return joblib.load("./estimator.pk1")



