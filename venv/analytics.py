# LFBrain - análise dos concursos da lotofácil utilizando o SciKit-Learn e tensorflow (skflow)
# Módulo de análise
# by Amaury Carvalho
# junho de 2018

from sklearn import svm, metrics
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn import linear_model
from sklearn.neighbors import KNeighborsRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
import csv
import skflow
import numpy as np

# inicio

print("Analisando o histórico dos concursos da lotofácil")

print("Carregando o arquivo d_lotfac.csv...")

with open("./d_lotfac.csv", "r") as input:
    reader = csv.reader(input, lineterminator='\n')
    list = list(reader)

print(list)

# criando o dataset (apenas a menor dezena de cada concurso)
# x (train) = [ concurso atual, dezena do concurso anterior ]
# y (target) = dezena do concurso atual

print("Criando o dataset X e Y...")

x = []
y = []
i = 0
a = 0

for row in list:
    if(i > 0):
        x.append( [i, a] )
        row[0]=0
        row[1]=0
        for k in range(len(row)):
            row[k] = int(row[k])
        row.sort()
        a=row[2]
        y.append(a)
    i += 1

scaler = StandardScaler()
scaler.fit(x)
x_scaled = scaler.transform(x)

print("(X) Concurso atual versus o resultado do anterior")
print(x)

print("(Y) Resultado do concurso atual")
print(y)

def testScore( clf ):
    lines = clf.__doc__.splitlines()
    title = lines[0]
    if title == '':
        title = lines[1]
    print('\n', title.strip())
    clf.fit(x_scaled, y)
    #score = metrics.accuracy_score(y, clf.predict(x))
    score = clf.score( x_scaled, y )
    print("=Score: %f" % score)

# estimators

estimators = [ svm.SVC(gamma=0.001, C=100.),
               LogisticRegression(),
               GaussianNB(),
               linear_model.SGDRegressor( max_iter=1000, n_iter=None ),
               KNeighborsRegressor(),
               GaussianProcessRegressor(),
               MLPRegressor( hidden_layer_sizes=(300,150,100,50,25,10), max_iter=15000,
                             random_state=0, solver='lbfgs' ) ]
               #skflow.TensorFlowDNNRegressor(hidden_units=[10, 20, 10], n_classes=3) ]

for estimator in estimators:
    testScore( estimator )

#print("Prevendo resultado do concurso 9 com base no resultado do concurso 8=")
#print(clf.predict([[9, 1]]))

# classificação rede neural

#print("Classificação TensorFlow (skflow, Linear Classifier)")

#import skflow
#import numpy as np

#def input_fn(X):
#    return tf.split(1, 2, X)

#clf = skflow.TensorFlowRNNClassifier(rnn_size=2, cell_type='lstm', n_classes=2, input_op_fn=input_fn)
#clf = skflow.TensorFlowDNNClassifier(hidden_units=[10, 20, 10], n_classes=3)
#clf = skflow.TensorFlowLinearClassifier(n_classes=3)

#data = np.array(x)
#labels = np.array(y)
#clf.fit(data, labels)

#score = metrics.accuracy_score(y, clf.predict(x))
#print("=Acuracidade: %f" % score)
