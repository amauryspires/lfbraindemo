# LFBrain - análise dos concursos da lotofácil utilizando o SciKit-Learn e tensorflow (skflow)
# Módulo de download dos concursos da Caixa Economica Federal
# by Amaury Carvalho
# junho de 2018

import requests
import wget

# baixando os últimos concursos

print("Baixando o histórico dos concursos da lotofácil...")
file = requests.get("http://www1.caixa.gov.br/loterias/_arquivos/loterias/D_lotfac.zip")

# descompactando o arquivo

from io import BytesIO
from zipfile import ZipFile

zipfile = ZipFile(BytesIO(file.content))

print("Arquivo(s) de histórico dos concursos:")
print(zipfile.namelist())

# carregando os concursos na memória

unzip = zipfile.open("D_LOTFAC.HTM").read()

# fazendo o parse do html

print("Lendo o arquivo do histórico dos concursos...")
import pandas

parsed = pandas.read_html(unzip)
df = pandas.concat(parsed)

list = []

for row in df.iterrows():
    if(not row[1][2] != row[1][2]):
        if(row[1][0]=="Concurso"):
            list.append(
                [row[1][0], row[1][1], row[1][2],  row[1][3],  row[1][4],  row[1][5],  row[1][6],
                                       row[1][7],  row[1][8],  row[1][9],  row[1][10], row[1][11],
                                       row[1][12], row[1][13], row[1][14], row[1][15], row[1][16] ])
        else:
            list.append( [ int(row[1][0]), row[1][1], int(row[1][2]),  int(row[1][3]),  int(row[1][4]),  int(row[1][5]),  int(row[1][6]),
                                                      int(row[1][7]),  int(row[1][8]),  int(row[1][9]),  int(row[1][10]), int(row[1][11]),
                                                      int(row[1][12]), int(row[1][13]), int(row[1][14]), int(row[1][15]), int(row[1][16]) ] )

print(list)

# criando o csv

print("Criando o arquivo d_lotfac.csv...")

import csv

with open("./d_lotfac.csv", "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(list)

print("Arquivo criado com sucesso.")
